export { default as Foxy, FoxyDirection } from "../src/scripts/Foxy.js";
export { default as Fennecs } from "../src/scripts/Fennecs.js";
export { default as FoxParade } from "../src/scripts/FoxParade.js";