(function () {
	'use strict';

	/**
	 * Various class names that can be applied to the slideshow and slides
	 */
	let FoxyClassName =
	{
		initialised: "foxy-initialised",
		hasBullets: "foxy-has-bullets",
		current: "foxy-current",
		departed: "foxy-departed",
		arrows: "foxy-arrows",
		arrow: "foxy-arrow",
		previousArrow: "foxy-prev",
		nextArrow: "foxy-next",
		bullets: "foxy-bullets",
		bulletItem: "foxy-bullet-item",
		bullet: "foxy-bullet",
		activeBullet: "foxy-active",
		noAnimation: "foxy-no-animation",
		dragging: "foxy-dragging"
	};

	let defaultOptions$2 =
	{
		arrows: true,
		arrowsParent: null,
		logging: false,
		timer: 3000
	};

	let ignoredChildren = [FoxyClassName.arrows, FoxyClassName.bullets];

	/**
	 * Base logic for the Foxy slideshows
	 * @abstract
	 */
	class Slideshow
	{
		static defaultOptions = defaultOptions$2;
		static instances = new WeakMap;

		/**
		 * Initialises slideshow on a particular element, elements or selector
		 * @param	{HTMLElement|HTMLElement[]|string}	elements	The element or elements to apply the slideshow to
		 * @param	{Object}							options		The list of options to pass to the slideshow
		 */
		static init(elements, options = {})
		{
			let finalOptions = {};
			Object.assign(finalOptions, this.defaultOptions, options);

			if(typeof elements === "string")
			{
				Slideshow.log(`CSS selector passed, searching for '${elements}'`, options);
				return this.init(Array.from(document.querySelectorAll(elements)), finalOptions);
			}
			else if(Array.isArray(elements))
			{
				Slideshow.log(`Initialising ${elements.length} slideshows`, options);
				let foxies = [];

				for(let element of elements)
				{
					foxies.push(this.init(element, finalOptions));
				}

				return foxies;
			}
			else if(elements instanceof HTMLElement)
			{
				return new this(elements, finalOptions);
			}
			else
			{
				throw new Error("Element must be an HTML element, an array of HTML elements, or a query string");
			}
		}

		/**
		 * Gets the slideshow instance for a specific element
		 * @param	{HTMLElement|string}	element		The element to get the slideshow instance for, or a selector
		 * @return	{Slideshow|undefined}				Either the slideshow instance (for the first element if using a selector), or undefined if there is no slideshow instance for that element
		 */
		static get(element)
		{
			if(element instanceof HTMLElement)
			{
				return this.instances.get(element);
			}

			/** @var {HTMLElement} */
			let actualElement = document.querySelector(element);

			if(actualElement === undefined)
			{
				return undefined;
			}

			return this.get(actualElement);
		}

		/**
		 * Logs a message to console.log, if logging is turned on in options
		 * @param	{string}	message		The message to log
		 * @param	{Object}	options		The options to use
		 */
		static log(message, options = defaultOptions$2)
		{
			if(options.logging)
			{
				console.log(message);
			}
		}

		/**
		 * Creates a new slideshow instance
		 * @param	{HTMLElement}	element		The element to apply the slideshow to
		 * @param	{Object}		options		The options to give the element
		 */
		constructor(element, options)
		{
			Slideshow.log("Initialising slideshow on element", options);

			this.element = element;
			this.options = options;

			Slideshow.instances.set(element, this);

			this.dimensionsUpdated();

			this.next();
			this.resetTimer();

			if(this.options.arrows)
			{
				this.setupArrows();
			}

			this.element.classList.add(FoxyClassName.initialised);

			let awaitingNextFrame = false;

			this.resizeHandler = () =>
			{
				if(awaitingNextFrame)
				{
					return;
				}

				awaitingNextFrame = true;

				window.requestAnimationFrame(() =>
				{
					awaitingNextFrame = false;
					this.dimensionsUpdated();
				});
			};

			this.log("Adding resize handler and observer");

			window.addEventListener("resize", this.resizeHandler);
			// noinspection JSUnresolvedFunction
			this.resizeObserver = new ResizeObserver(this.resizeHandler);
			this.resizeObserver.observe(this.element, { box: "border-box" });
		}

		/**
		 * Sets an option to a new value
		 * @param	{string}	option	The option to set
		 * @param	{any}		value	The value to set it to
		 * @return	{this}				This slideshow
		 */
		set(option, value)
		{
			if(!Object.hasOwn(this.options, option))
			{
				throw new Error(`${option} is not a valid option for this slideshow`);
			}

			this.options[option] = value;

			if(["arrows", "arrowsParent"].includes(option))
			{
				this.removeArrows();
				if(this.options.arrows) this.setupArrows();
			}
			else if(option === "timer")
			{
				this.resetTimer();
			}

			return this;
		}

		/**
		 * Logs a message to console.log, if logging is turned on in options
		 * @param	{string}	message		The message to log
		 */
		log(message)
		{
			if(this.options.logging)
			{
				console.log(message);
			}
		}

		/**
		 * Triggers whenever the dimensions of the slideshow might have changed
		 */
		dimensionsUpdated()
		{
			this.log("Recalculating dimensions");
		}

		/**
		 * Sets up the arrow buttons
		 */
		setupArrows()
		{
			this.log("Setting up arrows");

			let arrowsElement;

			if(this.options.arrowsParent === null)
			{
				this.log("No parent element provided, adding arrows to container element");
				arrowsElement = document.createElement("div");
				this.element.appendChild(arrowsElement);
			}
			else if(typeof this.options.arrowsParent === "string")
			{
				this.log(`CSS selector provided, searching for arrows parent element "${this.options.arrowsParent}`);
				arrowsElement = document.querySelector(this.options.arrowsParent);

				if(arrowsElement === null)
				{
					console.warn(`The arrowsParent selector '${this.options.arrowsParent} matched no elements on the page.`);
					return;
				}

				this.log("Found arrows parent element");
			}
			else if(this.options.arrowsParent instanceof HTMLElement)
			{
				arrowsElement = this.options.arrowsParent;
			}
			else
			{
				throw new Error("The arrowsParent option must be a DOM selector string, an HTMLElement or null");
			}

			let previousArrow = document.createElement("button");
			let nextArrow = document.createElement("button");

			previousArrow.innerText = "Previous";
			nextArrow.innerText = "Next";

			arrowsElement.classList.add(FoxyClassName.arrows);
			previousArrow.classList.add(FoxyClassName.arrow, FoxyClassName.previousArrow);
			nextArrow.classList.add(FoxyClassName.arrow, FoxyClassName.nextArrow);

			arrowsElement.append(previousArrow, nextArrow);

			previousArrow.addEventListener("click", (event) =>
			{
				event.preventDefault();
				this.previous();
			});

			nextArrow.addEventListener("click", (event) =>
			{
				event.preventDefault();
				this.next();
			});

			if(this.options.arrowsParent !== null)
			{
				this.generatedArrows = [previousArrow, nextArrow];
			}
		}

		/**
		 * Gets the child elements for this element
		 * @return	{HTMLElement[]}		The child elements
		 */
		getElements()
		{
			let elements = [];

			for(let child of this.element.children)
			{
				let contains = false;

				for(let ignoredClass of ignoredChildren)
				{
					if(child.classList.contains(ignoredClass))
					{
						contains = true;
						break;
					}
				}

				if(!contains)
				{
					elements.push(child);
				}
			}

			return elements;
		}

		/**
		 * Resets the timer, so that the timer won't trigger immediately after a manual transition
		 */
		resetTimer()
		{
			if(this.timerId !== undefined)
			{
				this.log("Clearing current timer");
				window.clearTimeout(this.timerId);
			}

			if(this.options.timer)
			{
				this.log(`Starting new timer for ${this.options.timer} milliseconds`);

				this.timerId = window.setInterval(() =>
				{
					// If the original element has been removed from the DOM, we'll want to deinitialise Foxy so it can be removed from memory
					if(Foxy.instances.get(this.element) === undefined)
					{
						this.deinit();
						return;
					}

					this.next();
				}, this.options.timer);
			}
		}

		/**
		 * Gets the current elements
		 * @abstract
		 * @return	{HTMLElement[]}		The current elements
		 */
		getCurrent()
		{
			throw new Error("Slideshow must implement getCurrent()");
		}

		/**
		 * Moves to a specific slide
		 * @abstract
		 * @param	{HTMLElement}		slide		The element, or the slide index to move to
		 * @return	{Promise<void>}					After the move has completed
		 */
		async moveTo(slide)
		{
			throw new Error("Slideshow must implement moveTo()");
		}

		/**
		 * Moves to the next slide
		 * @abstract
		 * @return	{Promise<void>}		After the move has completed
		 */
		async next()
		{
			throw new Error("Slideshow must implement next()")
		}

		/**
		 * Moves to the previous slide
		 * @abstract
		 * @return	{Promise<void>}		After the move has completed
		 */
		async previous()
		{
			throw new Error("Slideshow must implement previous()")
		}

		/**
		 * Removes all foxy classes from this element, deletes any foxy elements, and cleans up any dangling variables
		 */
		deinit()
		{
			this.log("Deinitialising slideshow");

			let elements = this.getElements();

			this.element.classList.remove(`foxy-${this.options.animation}`);

			for(let key in Object.keys(FoxyClassName))
			{
				this.element.classList.remove(FoxyClassName[key]);

				for(let element of elements)
				{
					element.classList.remove(FoxyClassName[key]);
				}
			}

			for(let ignoredChildClass of ignoredChildren)
			{
				this.log(`Removing "${ignoredChildClass} elements from container class`);

				for(let item of this.element.getElementsByClassName(ignoredChildClass))
				{
					item.remove();
				}
			}

			this.removeArrows();

			if(this.timerId !== undefined)
			{
				this.log("Clearing timer");

				window.clearInterval(this.timerId);
			}

			window.removeEventListener("resize", this.resizeHandler);
			this.resizeObserver.disconnect();

			Slideshow.instances.delete(this.element);
		}

		/**
		 * Removes arrows from this slideshow
		 */
		removeArrows()
		{
			if(this.generatedArrows !== undefined)
			{
				this.log("Removing arrows");

				for(let arrow of this.generatedArrows)
				{
					arrow.remove();
				}
			}
		}
	}

	/**
	 * The directions that an animation can move in
	 */
	let FoxyDirection =
	{
		forward: "foxy-direction-forward",
		backward: "foxy-direction-backward",
		default: "foxy-direction-default"
	};

	let defaultOptions$1 = Object.assign(
	{
		animation: "slide",
		bullets: true,
		bulletsParent: null,
		onMove: undefined,
		dragging: true
	}, Slideshow.defaultOptions);

	/**
	 * The base foxy class, handles most of the slideshow logic
	 */
	let Foxy$1 = class Foxy extends Slideshow
	{
		static defaultOptions = defaultOptions$1;
		static baseClass = "foxy";

		/**
		 * Creates a new Foxy instance
		 * @param	{HTMLElement}	element		The element to apply foxy to
		 * @param	{Object}		options		The options to give the element
		 */
		constructor(element, options)
		{
			super(element, options);

			if(!this.element.classList.contains(this.constructor.baseClass))
			{
				this.element.classList.add(this.constructor.baseClass);
				console.warn(`Element missing base '${this.constructor.baseClass}' class, added automatically. This class should already be applied, to avoid unnecessary DOM updates.`);
			}

			this.element.classList.add(`foxy-${options.animation}`);

			if(this.options.dragging)
			{
				this.setupDragHandlers();
			}

			this.element.classList.add(FoxyClassName.noAnimation);
		}

		set(option, value)
		{
			let currentAnimation = this.options.animation;

			super.set(option, value);

			if(option === "animation")
			{
				this.element.classList.remove(`foxy-${currentAnimation}`);
				this.element.classList.add(`foxy-${value}`);
			}
			else if(["bullets", "bulletsParent"].includes(option))
			{
				this.removeBullets();
				if(this.options.bullets) this.setupBullets();
			}
			else if(option === "dragging")
			{
				this.removeDragHandlers();
				if(value) this.setupDragHandlers();
			}

			return this;
		}

		/**
		 * Triggers whenever the dimensions of the slideshow might have changed
		 */
		dimensionsUpdated()
		{
			super.dimensionsUpdated();

			this.setCssDimensions();

			if(this.options.bullets)
			{
				this.setupBullets();
			}
		}

		/**
		 * Passes the width of the container to CSS, for the purpose of animations
		 */
		setCssDimensions()
		{
			let width = this.element.clientWidth;
			let height = this.element.clientHeight;
			this.element.style.setProperty("--full-width", `${width}px`);
			this.element.style.setProperty("--full-height", `${height}px`);
		}

		/**
		 * Sets up, or resets, the slide bullets
		 */
		setupBullets()
		{
			this.log("Setting up bullets");

			let bulletsElement;

			if(this.options.bulletsParent === null)
			{
				this.log(`No bullets parent provided`);
				bulletsElement = this.element.getElementsByClassName(FoxyClassName.bullets).item(0);

				if(bulletsElement === null)
				{
					this.log("Creating new bullets parent");
					bulletsElement = document.createElement("div");
					bulletsElement.classList.add(FoxyClassName.bullets);
					this.element.appendChild(bulletsElement);
				}
			}
			else if(typeof this.options.bulletsParent === "string")
			{
				this.log(`CSS selector provided, searching for bullets parent element "${this.options.bulletsParent}`);
				bulletsElement = document.querySelector(this.options.bulletsParent);

				if(bulletsElement === null)
				{
					console.warn(`The bulletsParent selector '${this.options.bulletsParent} matched no elements on the page.`);
					return;
				}

				this.log("Found bullets parent element");
			}
			else if(this.options.bulletsParent instanceof HTMLElement)
			{
				bulletsElement = this.options.arrowsParent;
			}
			else
			{
				throw new Error("The bulletsParent option must be a DOM selector string, an HTMLElement or null");
			}

			if(this.generatedBullets !== undefined)
			{
				this.generatedBullets.forEach((bullet) => bullet.remove());
			}

			this.generatedBullets = [];

			for(let i = 0; i < this.getSlideCount(); i += 1)
			{
				let index = i;
				let listItem = document.createElement("li");
				let button = document.createElement("button");

				listItem.classList.add(FoxyClassName.bulletItem);
				button.classList.add(FoxyClassName.bullet);

				listItem.appendChild(button);
				bulletsElement.appendChild(listItem);

				button.innerText = String(i + 1);

				button.addEventListener("click", (event) =>
				{
					event.preventDefault();

					this.moveTo(index);
				});

				this.generatedBullets.push(listItem);
			}

			this.updateCurrentBullet();
		}

		/**
		 * Sets up the drag handlers
		 */
		setupDragHandlers()
		{
			this.log("Setting up dragging support");

			let initialX = undefined;
			let initialY = undefined;
			let elementHeight = undefined;
			let elementWidth = undefined;
			let dragMultiplier = undefined;
			let initialSlide = undefined;
			let currentOffset = undefined;
			let startTime = undefined;

			let clamp = (min, value, max) => Math.min(Math.max(min, value), max);

			// Touch events can involve multiple fingers, so we'll use a slightly different method for accessing pageX/pageY
			let getPointFromEvent = (event) =>
			{
				if(event instanceof MouseEvent)
				{
					return [event.pageX, event.pageY];
				}
				else if(event instanceof TouchEvent)
				{
					for(let touch of event.touches)
					{
						if(touch.pageX !== undefined && touch.pageY !== undefined)
						{
							return [touch.pageX, touch.pageY];
						}
					}
				}

				return [0, 0];
			};

			let onDragStart = (event) =>
			{
				// Make sure we're not right-clicking here
				if(event.button !== undefined && event.button !== 0) return;

				this.log("Starting drag");

				[initialX, initialY] = getPointFromEvent(event);
				startTime = Date.now();

				elementWidth = this.element.getBoundingClientRect().width;
				elementHeight = this.element.getBoundingClientRect().height;

				dragMultiplier = 0;
				currentOffset = 0;
				initialSlide = this.getCurrentIndex();
				this.element.classList.add(FoxyClassName.dragging);

				if(this.timerId !== undefined)
				{
					this.log("Pausing timer");
					window.clearInterval(this.timerId);
				}
			};

			let onDrag = async (event) =>
			{
				if(initialX === undefined) return; // This event doesn't apply to this element

				let [pageX, pageY] = getPointFromEvent(event);
				let differenceX = initialX - pageX;
				let differenceY = initialY - pageY;

				let percentageX = differenceX / elementWidth;
				let percentageY = differenceY / elementHeight;

				dragMultiplier = clamp(-1, percentageX + percentageY, 1);
				let desiredOffset = currentOffset;

				// We need to be animating in a specific direction
				if(dragMultiplier > 0)
				{
					desiredOffset = 1;
				}
				else if(dragMultiplier < 0)
				{
					desiredOffset = -1;
					dragMultiplier = Math.abs(dragMultiplier);
				}

				while(desiredOffset > currentOffset)
				{
					this.log("Incrementing slide to match drag direction");

					await this.next();
					currentOffset += 1;
				}

				while(desiredOffset < currentOffset)
				{
					this.log("Decrementing slide to match drag direction");

					await this.previous();
					currentOffset -= 1;
				}

				this.log(`Setting dragMultiplier to ${dragMultiplier}`);
				this.element.style.setProperty("--drag-multiplier", String(dragMultiplier));
			};

			let onDragEnd = async (event) =>
			{
				if(initialX === undefined) return; // This event doesn't apply to this element

				this.log("Finishing drag");

				let [pageX, pageY] = getPointFromEvent(event);
				let differenceX = initialX - pageX;
				let differenceY = initialY - pageY;
				let timeDifference = Date.now() - startTime;

				// If the user has dragged the element more than half of the width of the element
				let isFullDrag = Math.abs(dragMultiplier) > 0.2;

				// If the user has made a "fling" gesture
				let isFling = Math.max(Math.abs(differenceX), Math.abs(differenceY)) > 50 && timeDifference < 300;

				initialX = undefined;
				initialY = undefined;
				startTime = undefined;

				if(!isFullDrag && !isFling)
				{
					this.log("Partial drag, resetting");

					// We're assuming that the user has given up on dragging, so we'll just reverse the animation
					await this.moveTo(initialSlide);
					this.log(`Setting dragMultiplier to ${dragMultiplier}`);
					this.element.style.setProperty("--drag-multiplier", String(1 - dragMultiplier));
				}

				this.element.classList.remove(FoxyClassName.dragging);
				dragMultiplier = undefined;
				this.resetTimer();
			};

			/** @var {Map<EventTarget, Map<string, (MouseEvent) => void>>} dragHandlers */
			let dragHandlers = new Map();
			dragHandlers.set(window, new Map());
			dragHandlers.set(this.element, new Map());

			dragHandlers.get(this.element).set("mousedown", onDragStart);
			dragHandlers.get(window).set("mousemove", onDrag);
			dragHandlers.get(window).set("mouseup", onDragEnd);

			dragHandlers.get(this.element).set("touchstart", onDragStart);
			dragHandlers.get(window).set("touchmove", onDrag);
			dragHandlers.get(window).set("touchend", onDragEnd);

			dragHandlers.forEach((events, target) =>
			{
				events.forEach((handler, event) => target.addEventListener(event, handler));
			});

			this.dragHandlers = dragHandlers;
		}

		/**
		 * Sets the current bullet
		 */
		updateCurrentBullet()
		{
			if(this.generatedBullets === undefined || this.generatedBullets.length === 0)
			{
				return;
			}

			this.log("Updating current bullet");

			let bulletItems = this.generatedBullets;
			let bullets = bulletItems.map((bulletItem) => bulletItem.children.item(0));

			for(let i = 0; i < bulletItems.length; i += 1)
			{
				bulletItems[i].classList.remove(FoxyClassName.activeBullet);
				bullets[i].disabled = false;
			}

			let currentIndex = this.getCurrentIndex();

			if(currentIndex !== undefined)
			{
				this.log(`Setting current bullet to bullet ${currentIndex}`);
				bulletItems[currentIndex].classList.add(FoxyClassName.activeBullet);
				bullets[currentIndex].disabled = true;
			}
		}

		/**
		 * Gets the elements in a particular slide
		 * @param	{number}			slide	A specific slide to limit the elements to
		 * @return	{HTMLElement[]}				The elements in that slide
		 */
		getElementsInSlide(slide)
		{
			let elements = this.getElements();

			return [elements[slide]];
		}

		/**
		 * Gets the number of slides in this slideshow
		 * @return	{number}	The number of slides
		 */
		getSlideCount()
		{
			return this.getElements().length;
		}

		/**
		 * Gets the number of elements per slide
		 * @return	{number}	The number of elements per slide
		 */
		getPerSlide()
		{
			return 1;
		}

		/**
		 * Gets the current elements
		 * @return	{HTMLElement[]}		The current elements
		 */
		getCurrent()
		{
			let current = this.element.getElementsByClassName(FoxyClassName.current);
			let result = [];

			for(let i = 0; i < current.length; i += 1)
			{
				result.push(current.item(i));
			}

			return result;
		}

		/**
		 * Gets the slide index of a specific element
		 * @param	{HTMLElement}		element		The elemeent to get the index for
		 * @return	{number|undefined}				The 0-based index, or undefined if that element isn't in this slideshow
		 */
		getIndex(element)
		{
			let elements = this.getElements();
			let index = elements.indexOf(element);

			if(index < 0)
			{
				return undefined;
			}

			return index;
		}

		/**
		 * Gets the index of the current slide
		 * @return	{number[]}	The 0-based indexes
		 */
		getCurrentIndex()
		{
			let current = this.getCurrent()[0];

			if(current === undefined)
			{
				return undefined;
			}

			return this.getIndex(current);
		}

		/**
		 * Moves to a specific slide
		 * @param	{HTMLElement|number|string}		slide		The element, or the slide index to move to
		 * @param	{string}						direction	The direction to move in
		 * @return	{Promise<void>}								After the move has completed
		 */
		async moveTo(slide, direction = FoxyDirection.default)
		{
			if(typeof slide === "number")
			{
				if(this.getSlideCount() > slide)
				{
					this.log(`Moving to slide ${slide}`);
					await this.moveTo(this.getElementsInSlide(slide)[0], direction);
				}
				else
				{
					this.log(`Slide ${slide} does not exist, skipping`);
				}

				return;
			}
			else if(typeof slide === "string")
			{
				this.log(`Using selector to search for slide "${slide}"`);

				/** @var {HTMLElement} */
				let actualElement = document.querySelector(slide);

				if(actualElement === undefined)
				{
					this.log("No slide found, skipping");
					return;
				}

				await this.moveTo(actualElement, direction);

				return;
			}

			let current = this.getCurrent();

			// Don't attempt to move to the current slide
			if(current[0] === slide)
			{
				this.log("Attempting to move to current slide, skipping");
				return;
			}

			// Remove the animation disabler, now that we've definitely moved to another slide
			this.element.classList.remove(FoxyClassName.noAnimation);

			// Make sure the effects of dragging have been removed
			this.log(`Setting dragMultiplier to 0`);
			this.element.style.setProperty("--drag-multiplier", String(0));

			let slideIndex = this.getIndex(slide);
			let currentIndex = this.getCurrentIndex();

			if(direction === FoxyDirection.default)
			{
				if(slideIndex === undefined || currentIndex === undefined || currentIndex < slideIndex)
				{
					this.log("Default animation selected. Moving forward");
					direction = FoxyDirection.forward;
				}
				else
				{
					this.log("Default animation selected. Moving backward");
					direction = FoxyDirection.backward;
				}
			}

			for(let key of Object.keys(FoxyDirection))
			{
				this.element.classList.remove(FoxyDirection[key]);
			}

			this.element.classList.add(direction);
			let elements = this.getElements();

			for(let element of elements)
			{
				element.classList.remove(FoxyClassName.departed);
			}

			this.resetTimer();

			for(let element of elements)
			{
				element.classList.remove(FoxyClassName.current);
			}

			if(current !== undefined)
			{
				for(let element of current)
				{
					element.classList.add(FoxyClassName.departed);
				}
			}

			let slideElements = this.getElementsInSlide(this.getIndex(slide));

			for(let element of slideElements)
			{
				element.classList.add(FoxyClassName.current);
			}

			if(this.options.onMove !== undefined)
			{
				this.options.onMove(currentIndex, slideIndex, direction);
			}

			this.updateCurrentBullet();
		}

		/**
		 * Moves to the next slide
		 * @return	{Promise<void>}		After the move has completed
		 */
		async next()
		{
			let currentIndex = this.getCurrentIndex();
			let total = this.getSlideCount();

			if(currentIndex === undefined || currentIndex >= total - 1)
			{
				this.log("Moving to first slide");
				await this.moveTo(0, FoxyDirection.forward);
			}
			else
			{
				this.log("Moving to next slide");
				await this.moveTo(currentIndex + 1, FoxyDirection.forward);
			}
		}

		/**
		 * Moves to the previous slide
		 * @return	{Promise<void>}		After the move has completed
		 */
		async previous()
		{
			let currentIndex = this.getCurrentIndex();
			let total = this.getSlideCount();

			if(currentIndex === undefined)
			{
				this.log("Moving to first slide");
				await this.moveTo(0, FoxyDirection.backward);
			}
			else if(currentIndex <= 0)
			{
				this.log("Moving to last slide");
				await this.moveTo(total - 1, FoxyDirection.backward);
			}
			else
			{
				this.log("Moving to previous slide");
				await this.moveTo(currentIndex - 1, FoxyDirection.backward);
			}
		}

		/**
		 * Removes all foxy classes from this element, deletes any foxy elements, and cleans up any dangling variables
		 */
		deinit()
		{
			super.deinit();

			let elements = this.getElements();

			for(let key in Object.keys(FoxyClassName))
			{
				this.element.classList.remove(FoxyClassName[key]);

				for(let element of elements)
				{
					element.classList.remove(FoxyClassName[key]);
				}
			}

			for(let key in Object.keys(FoxyDirection))
			{
				this.element.classList.remove(FoxyClassName[key]);
			}

			this.removeBullets();
			this.removeDragHandlers();
		}

		/**
		 * Removes bullets from this slideshow
		 */
		removeBullets()
		{
			if(this.generatedBullets !== undefined)
			{
				this.log("Removing bullets");

				for(let bullet of this.generatedBullets)
				{
					bullet.remove();
				}
			}
		}

		/**
		 * Removes drag handlers from this slideshow
		 */
		removeDragHandlers()
		{
			if(this.dragHandlers !== undefined)
			{
				this.dragHandlers.forEach((events, target) =>
				{
					events.forEach((handler, event) => target.removeEventListener(event, handler));
				});
			}
		}
	};

	let defaultOptions = Object.assign(
	{
		rows: undefined
	}, Foxy$1.defaultOptions);

	/**
	 * Fennecs specific class, handles grid logic
	 */
	class Fennecs extends Foxy$1
	{
		static defaultOptions = defaultOptions;
		static baseClass = "fennecs";

		/**
		 * Creates a new Foxy instance
		 * @param	{HTMLElement}	element		The element to apply foxy to
		 * @param	{Object}		options		The options to give the element
		 */
		constructor(element, options)
		{
			// Need to set this first
			if(options.rows !== undefined)
			{
				element.style.setProperty("--rows", options.rows);
			}

			if(options.bullets)
			{
				element.classList.add(FoxyClassName.hasBullets);
			}

			super(element, options);
		}

		set(option, value)
		{
			super.set(option, value);

			if(option === "rows")
			{
				this.reorganise();
			}

			return this;
		}

		/**
		 * Triggers whenever the dimensions of the slideshow might have changed
		 */
		dimensionsUpdated()
		{
			this.reorganise();

			super.dimensionsUpdated();
		}

		/**
		 * Gets the number of columns in a row
		 * @return	{Number}	The number of columns
		 */
		getColumns()
		{
			let computed = window.getComputedStyle(this.element).gridTemplateColumns;
			return computed.split(" ").length;
		}

		/**
		 * Gets the number of rows to display
		 * @return	{Number}	The number of rows
		 */
		getRows()
		{
			let rows = window.getComputedStyle(this.element).getPropertyValue("--rows");
			return rows === undefined ? 1 : Number(rows);
		}

		/**
		 * Reorganises elements
		 */
		reorganise()
		{
			this.log("Reorganising cells");

			let currentElements = this.getCurrent();

			let elements = this.getElements();

			// Reset slide overlap
			for(let slide of elements)
			{
				slide.style.setProperty("--row", "auto");
				slide.style.setProperty("--column", "auto");
			}

			let columns = this.getColumns();
			let perSlide = this.getPerSlide();

			this.log(`Generating a ${columns}x${this.getRows()} grid`);

			// Assign slides to overlapping cells
			elements.forEach((slide, index) =>
			{
				let repeatingIndex = index % perSlide;
				let column = repeatingIndex % columns;
				let row = (repeatingIndex - column) / columns;

				slide.style.setProperty("--row", String(row + 1));
				slide.style.setProperty("--column", String(column + 1));
			});

			// Handle applying and removing "current" and "departed" classes, so that the correct elements are currently visible
			let updatedElements = this.getElementsInSlide(this.getCurrentIndex());
			let departedElements = this.element.getElementsByClassName(FoxyClassName.departed);

			currentElements.forEach((element) => element.classList.remove(FoxyClassName.current, FoxyClassName.departed));
			updatedElements.forEach((element) => element.classList.add(FoxyClassName.current));

			// We only want the "departed" animation to apply to the number of elements in a slide
			for(let i = 0; i < Math.min(departedElements.length, currentElements.length - updatedElements.length); i += 1)
			{
				departedElements.item(i).removeAttribute(FoxyClassName.departed);
			}
		}

		/**
		 * Gets the number of elements per slide
		 * @return	{number}	The number of elements per slide
		 */
		getPerSlide()
		{
			let columns = this.getColumns();
			let rows = this.getRows();
			return rows * columns;
		}

		getElementsInSlide(slide)
		{
			let perSlide = this.getPerSlide();
			return this.getElements().slice(slide * perSlide, slide * perSlide + perSlide);
		}

		getSlideCount()
		{
			return Math.ceil(this.getElements().length / this.getPerSlide());
		}

		getIndex(element)
		{
			let baseIndex = super.getIndex(element);

			if(baseIndex === undefined)
			{
				return undefined;
			}

			return Math.floor(baseIndex / this.getPerSlide());
		}

		deinit()
		{
			for(let element of this.getElements())
			{
				element.style.setProperty("--row", "");
				element.style.setProperty("--column", "");
			}

			super.deinit();

			this.log("Removing resize handler and observer");
			this.resizeObserver.disconnect();
			window.removeEventListener("resize", this.resizeHandler);
		}
	}

	class FoxParade extends Slideshow
	{
		/**
		 * Creates a new slideshow instance
		 * @param	{HTMLElement}	element		The element to apply the slideshow to
		 * @param	{Object}		options		The options to give the element
		 */
		constructor(element, options)
		{
			super(element, options);

			this.setupIntersectionObserver();
		}

		/**
		 * Sets up the intersection observer, so we can track which elements are currently visible
		 */
		setupIntersectionObserver()
		{
			let options =
			{
				root: this.element,
				threshold: [0, 0.99]
			};

			this.observer = new IntersectionObserver((entries) =>
			{
				for(let entry of entries)
				{
					entry.target.classList.remove(FoxyClassName.current);
					entry.target.classList.remove(FoxyClassName.departed);

					if(entry.intersectionRatio >= 0.99)
					{
						entry.target.classList.add(FoxyClassName.current);
					}
					else if(entry.isIntersecting)
					{
						entry.target.classList.add(FoxyClassName.departed);
					}
				}
			}, options);

			for(let item of this.getElements())
			{
				this.observer.observe(item);
			}
		}

		dimensionsUpdated()
		{
			super.dimensionsUpdated();

			let elements = this.getElements();
			let last = elements[elements.length - 1];
			let box = last.getBoundingClientRect();
			let width = last.offsetLeft + box.width;
			let height = last.offsetTop + box.height;

			this.element.style.setProperty("--inner-width", `${width}px`);
			this.element.style.setProperty("--inner-height", `${height}px`);
		}

		getCurrent()
		{
			let elements = this.element.getElementsByClassName(FoxyClassName.current);

			// If none of the elements are completely visible, we'll fall back to the elements that are partially visible
			if(elements.length === 0) elements = this.element.getElementsByClassName(FoxyClassName.departed);

			// Just give up at this point and settle for the first element, assuming it exists
			if(elements.length === 0) elements = this.getElements().slice(0, 1);

			return [...elements];
		}

		/**
		 * Checks if this is meant to be a vertical slideshow or a horizontal one
		 * @return	{Boolean}	The vertical-ness
		 */
		isVertical()
		{
			return this.element.classList.contains("vertical");
		}

		async moveTo(slide)
		{
			if(!this.getElements().includes(slide))
			{
				this.log("This element is not contained in the slideshow, skipping");
				return;
			}

			let left = slide.offsetLeft;
			let top = slide.offsetTop;
			let box = slide.getBoundingClientRect();
			let width = box.width;
			let height = box.height;

			this.element.scrollTo({ left: left + (width / 2), top: top + (height / 2), behavior: "smooth" });
		}

		async next()
		{
			let all = this.getElements();
			let last = all[all.length - 1];

			let visible = this.getCurrent();
			let lastVisible = visible[visible.length - 1];

			if(last === lastVisible)
			{
				this.element.scrollTo({ left: 0, top: 0, behavior: "smooth" });
			}
			else
			{
				let box = this.element.getBoundingClientRect();
				let scrollBy = { behavior: "smooth" };

				if(this.isVertical()) scrollBy.top = box.height;
				else scrollBy.left = box.width;

				this.element.scrollBy(scrollBy);
			}
		}

		async previous()
		{
			let all = this.getElements();
			let first = all[0];

			let visible = this.getCurrent();
			let firstVisible = visible[0];

			if(first === firstVisible)
			{
				this.element.scrollTo({ left: Number.MAX_VALUE, top: Number.MAX_VALUE, behavior: "smooth" });
			}
			else
			{
				let box = this.element.getBoundingClientRect();
				let scrollBy = { behavior: "smooth" };

				if(this.isVertical()) scrollBy.top = -box.height;
				else scrollBy.left = -box.width;

				this.element.scrollBy(scrollBy);
			}
		}
	}

	window.Foxy = Foxy$1;
	window.FoxyDirection = FoxyDirection;
	window.Fennecs = Fennecs;
	window.FoxParade = FoxParade;

})();
