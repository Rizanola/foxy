window.Foxy.init(".js-foxy-instant", { animation: "instant" });
window.Foxy.init(".js-foxy-fade", { animation: "fade" });
window.Foxy.init(".js-foxy-fade-in", { animation: "fade-in" });
window.Foxy.init(".js-foxy-slide", { animation: "slide" });
window.Foxy.init(".js-foxy-vertical-slide", { animation: "vertical-slide" });
window.Foxy.init(".js-foxy-external-arrows", { arrowsParent: ".js-external-arrows", bulletsParent: ".js-external-bullets" });
window.Fennecs.init(".js-fennecs", { rows: 2 });
window.FoxParade.init(".js-fox-parade");