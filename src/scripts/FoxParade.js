import Slideshow, {FoxyClassName} from "./Slideshow.js";

export default class FoxParade extends Slideshow
{
	/**
	 * Creates a new slideshow instance
	 * @param	{HTMLElement}	element		The element to apply the slideshow to
	 * @param	{Object}		options		The options to give the element
	 */
	constructor(element, options)
	{
		super(element, options);

		this.setupIntersectionObserver();
	}

	/**
	 * Sets up the intersection observer, so we can track which elements are currently visible
	 */
	setupIntersectionObserver()
	{
		let options =
		{
			root: this.element,
			threshold: [0, 0.99]
		};

		this.observer = new IntersectionObserver((entries) =>
		{
			for(let entry of entries)
			{
				entry.target.classList.remove(FoxyClassName.current);
				entry.target.classList.remove(FoxyClassName.departed);

				if(entry.intersectionRatio >= 0.99)
				{
					entry.target.classList.add(FoxyClassName.current);
				}
				else if(entry.isIntersecting)
				{
					entry.target.classList.add(FoxyClassName.departed);
				}
			}
		}, options);

		for(let item of this.getElements())
		{
			this.observer.observe(item);
		}
	}

	dimensionsUpdated()
	{
		super.dimensionsUpdated();

		let elements = this.getElements();
		let last = elements[elements.length - 1];
		let box = last.getBoundingClientRect();
		let width = last.offsetLeft + box.width;
		let height = last.offsetTop + box.height;

		this.element.style.setProperty("--inner-width", `${width}px`);
		this.element.style.setProperty("--inner-height", `${height}px`);
	}

	getCurrent()
	{
		let elements = this.element.getElementsByClassName(FoxyClassName.current);

		// If none of the elements are completely visible, we'll fall back to the elements that are partially visible
		if(elements.length === 0) elements = this.element.getElementsByClassName(FoxyClassName.departed);

		// Just give up at this point and settle for the first element, assuming it exists
		if(elements.length === 0) elements = this.getElements().slice(0, 1);

		return [...elements];
	}

	/**
	 * Checks if this is meant to be a vertical slideshow or a horizontal one
	 * @return	{Boolean}	The vertical-ness
	 */
	isVertical()
	{
		return this.element.classList.contains("vertical");
	}

	async moveTo(slide)
	{
		if(!this.getElements().includes(slide))
		{
			this.log("This element is not contained in the slideshow, skipping");
			return;
		}

		let left = slide.offsetLeft;
		let top = slide.offsetTop;
		let box = slide.getBoundingClientRect();
		let width = box.width;
		let height = box.height;

		this.element.scrollTo({ left: left + (width / 2), top: top + (height / 2), behavior: "smooth" });
	}

	async next()
	{
		let all = this.getElements();
		let last = all[all.length - 1];

		let visible = this.getCurrent();
		let lastVisible = visible[visible.length - 1];

		if(last === lastVisible)
		{
			this.element.scrollTo({ left: 0, top: 0, behavior: "smooth" })
		}
		else
		{
			let box = this.element.getBoundingClientRect();
			let scrollBy = { behavior: "smooth" };

			if(this.isVertical()) scrollBy.top = box.height;
			else scrollBy.left = box.width;

			this.element.scrollBy(scrollBy);
		}
	}

	async previous()
	{
		let all = this.getElements();
		let first = all[0];

		let visible = this.getCurrent();
		let firstVisible = visible[0];

		if(first === firstVisible)
		{
			this.element.scrollTo({ left: Number.MAX_VALUE, top: Number.MAX_VALUE, behavior: "smooth" })
		}
		else
		{
			let box = this.element.getBoundingClientRect();
			let scrollBy = { behavior: "smooth" };

			if(this.isVertical()) scrollBy.top = -box.height;
			else scrollBy.left = -box.width;

			this.element.scrollBy(scrollBy);
		}
	}
}