import Foxy, {FoxyDirection} from "./Foxy.js";
import Fennecs from "./Fennecs.js";
import FoxParade from "./FoxParade.js";

window.Foxy = Foxy;
window.FoxyDirection = FoxyDirection;
window.Fennecs = Fennecs;
window.FoxParade = FoxParade