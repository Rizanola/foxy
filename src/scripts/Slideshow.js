/**
 * Various class names that can be applied to the slideshow and slides
 */
export let FoxyClassName =
{
	initialised: "foxy-initialised",
	hasBullets: "foxy-has-bullets",
	current: "foxy-current",
	departed: "foxy-departed",
	arrows: "foxy-arrows",
	arrow: "foxy-arrow",
	previousArrow: "foxy-prev",
	nextArrow: "foxy-next",
	bullets: "foxy-bullets",
	bulletItem: "foxy-bullet-item",
	bullet: "foxy-bullet",
	activeBullet: "foxy-active",
	noAnimation: "foxy-no-animation",
	dragging: "foxy-dragging"
};

let defaultOptions =
{
	arrows: true,
	arrowsParent: null,
	logging: false,
	timer: 3000
};

let ignoredChildren = [FoxyClassName.arrows, FoxyClassName.bullets];

/**
 * Base logic for the Foxy slideshows
 * @abstract
 */
export default class Slideshow
{
	static defaultOptions = defaultOptions;
	static instances = new WeakMap;

	/**
	 * Initialises slideshow on a particular element, elements or selector
	 * @param	{HTMLElement|HTMLElement[]|string}	elements	The element or elements to apply the slideshow to
	 * @param	{Object}							options		The list of options to pass to the slideshow
	 */
	static init(elements, options = {})
	{
		let finalOptions = {};
		Object.assign(finalOptions, this.defaultOptions, options);

		if(typeof elements === "string")
		{
			Slideshow.log(`CSS selector passed, searching for '${elements}'`, options);
			return this.init(Array.from(document.querySelectorAll(elements)), finalOptions);
		}
		else if(Array.isArray(elements))
		{
			Slideshow.log(`Initialising ${elements.length} slideshows`, options)
			let foxies = [];

			for(let element of elements)
			{
				foxies.push(this.init(element, finalOptions));
			}

			return foxies;
		}
		else if(elements instanceof HTMLElement)
		{
			return new this(elements, finalOptions);
		}
		else
		{
			throw new Error("Element must be an HTML element, an array of HTML elements, or a query string");
		}
	}

	/**
	 * Gets the slideshow instance for a specific element
	 * @param	{HTMLElement|string}	element		The element to get the slideshow instance for, or a selector
	 * @return	{Slideshow|undefined}				Either the slideshow instance (for the first element if using a selector), or undefined if there is no slideshow instance for that element
	 */
	static get(element)
	{
		if(element instanceof HTMLElement)
		{
			return this.instances.get(element);
		}

		/** @var {HTMLElement} */
		let actualElement = document.querySelector(element);

		if(actualElement === undefined)
		{
			return undefined;
		}

		return this.get(actualElement);
	}

	/**
	 * Logs a message to console.log, if logging is turned on in options
	 * @param	{string}	message		The message to log
	 * @param	{Object}	options		The options to use
	 */
	static log(message, options = defaultOptions)
	{
		if(options.logging)
		{
			console.log(message);
		}
	}

	/**
	 * Creates a new slideshow instance
	 * @param	{HTMLElement}	element		The element to apply the slideshow to
	 * @param	{Object}		options		The options to give the element
	 */
	constructor(element, options)
	{
		Slideshow.log("Initialising slideshow on element", options);

		this.element = element;
		this.options = options;

		Slideshow.instances.set(element, this);

		this.dimensionsUpdated();

		let ignored = this.next();
		this.resetTimer();

		if(this.options.arrows)
		{
			this.setupArrows();
		}

		this.element.classList.add(FoxyClassName.initialised);

		let awaitingNextFrame = false;

		this.resizeHandler = () =>
		{
			if(awaitingNextFrame)
			{
				return;
			}

			awaitingNextFrame = true;

			window.requestAnimationFrame(() =>
			{
				awaitingNextFrame = false;
				this.dimensionsUpdated();
			});
		}

		this.log("Adding resize handler and observer");

		window.addEventListener("resize", this.resizeHandler);
		// noinspection JSUnresolvedFunction
		this.resizeObserver = new ResizeObserver(this.resizeHandler);
		this.resizeObserver.observe(this.element, { box: "border-box" });
	}

	/**
	 * Sets an option to a new value
	 * @param	{string}	option	The option to set
	 * @param	{any}		value	The value to set it to
	 * @return	{this}				This slideshow
	 */
	set(option, value)
	{
		if(!Object.hasOwn(this.options, option))
		{
			throw new Error(`${option} is not a valid option for this slideshow`);
		}

		this.options[option] = value;

		if(["arrows", "arrowsParent"].includes(option))
		{
			this.removeArrows();
			if(this.options.arrows) this.setupArrows();
		}
		else if(option === "timer")
		{
			this.resetTimer();
		}

		return this;
	}

	/**
	 * Logs a message to console.log, if logging is turned on in options
	 * @param	{string}	message		The message to log
	 */
	log(message)
	{
		if(this.options.logging)
		{
			console.log(message);
		}
	}

	/**
	 * Triggers whenever the dimensions of the slideshow might have changed
	 */
	dimensionsUpdated()
	{
		this.log("Recalculating dimensions");
	}

	/**
	 * Sets up the arrow buttons
	 */
	setupArrows()
	{
		this.log("Setting up arrows");

		let arrowsElement;

		if(this.options.arrowsParent === null)
		{
			this.log("No parent element provided, adding arrows to container element");
			arrowsElement = document.createElement("div");
			this.element.appendChild(arrowsElement);
		}
		else if(typeof this.options.arrowsParent === "string")
		{
			this.log(`CSS selector provided, searching for arrows parent element "${this.options.arrowsParent}`);
			arrowsElement = document.querySelector(this.options.arrowsParent);

			if(arrowsElement === null)
			{
				console.warn(`The arrowsParent selector '${this.options.arrowsParent} matched no elements on the page.`);
				return;
			}

			this.log("Found arrows parent element");
		}
		else if(this.options.arrowsParent instanceof HTMLElement)
		{
			arrowsElement = this.options.arrowsParent;
		}
		else
		{
			throw new Error("The arrowsParent option must be a DOM selector string, an HTMLElement or null");
		}

		let previousArrow = document.createElement("button");
		let nextArrow = document.createElement("button");

		previousArrow.innerText = "Previous";
		nextArrow.innerText = "Next";

		arrowsElement.classList.add(FoxyClassName.arrows);
		previousArrow.classList.add(FoxyClassName.arrow, FoxyClassName.previousArrow);
		nextArrow.classList.add(FoxyClassName.arrow, FoxyClassName.nextArrow);

		arrowsElement.append(previousArrow, nextArrow);

		previousArrow.addEventListener("click", (event) =>
		{
			event.preventDefault();
			let ignored = this.previous();
		});

		nextArrow.addEventListener("click", (event) =>
		{
			event.preventDefault();
			let ignored = this.next();
		});

		if(this.options.arrowsParent !== null)
		{
			this.generatedArrows = [previousArrow, nextArrow];
		}
	}

	/**
	 * Gets the child elements for this element
	 * @return	{HTMLElement[]}		The child elements
	 */
	getElements()
	{
		let elements = [];

		for(let child of this.element.children)
		{
			let contains = false;

			for(let ignoredClass of ignoredChildren)
			{
				if(child.classList.contains(ignoredClass))
				{
					contains = true;
					break;
				}
			}

			if(!contains)
			{
				elements.push(child);
			}
		}

		return elements;
	}

	/**
	 * Resets the timer, so that the timer won't trigger immediately after a manual transition
	 */
	resetTimer()
	{
		if(this.timerId !== undefined)
		{
			this.log("Clearing current timer");
			window.clearTimeout(this.timerId);
		}

		if(this.options.timer)
		{
			this.log(`Starting new timer for ${this.options.timer} milliseconds`);

			this.timerId = window.setInterval(() =>
			{
				// If the original element has been removed from the DOM, we'll want to deinitialise Foxy so it can be removed from memory
				if(Foxy.instances.get(this.element) === undefined)
				{
					this.deinit();
					return;
				}

				let ignored = this.next();
			}, this.options.timer);
		}
	}

	/**
	 * Gets the current elements
	 * @abstract
	 * @return	{HTMLElement[]}		The current elements
	 */
	getCurrent()
	{
		throw new Error("Slideshow must implement getCurrent()");
	}

	/**
	 * Moves to a specific slide
	 * @abstract
	 * @param	{HTMLElement}		slide		The element, or the slide index to move to
	 * @return	{Promise<void>}					After the move has completed
	 */
	async moveTo(slide)
	{
		throw new Error("Slideshow must implement moveTo()");
	}

	/**
	 * Moves to the next slide
	 * @abstract
	 * @return	{Promise<void>}		After the move has completed
	 */
	async next()
	{
		throw new Error("Slideshow must implement next()")
	}

	/**
	 * Moves to the previous slide
	 * @abstract
	 * @return	{Promise<void>}		After the move has completed
	 */
	async previous()
	{
		throw new Error("Slideshow must implement previous()")
	}

	/**
	 * Removes all foxy classes from this element, deletes any foxy elements, and cleans up any dangling variables
	 */
	deinit()
	{
		this.log("Deinitialising slideshow");

		let elements = this.getElements();

		this.element.classList.remove(`foxy-${this.options.animation}`);

		for(let key in Object.keys(FoxyClassName))
		{
			this.element.classList.remove(FoxyClassName[key]);

			for(let element of elements)
			{
				element.classList.remove(FoxyClassName[key]);
			}
		}

		for(let ignoredChildClass of ignoredChildren)
		{
			this.log(`Removing "${ignoredChildClass} elements from container class`);

			for(let item of this.element.getElementsByClassName(ignoredChildClass))
			{
				item.remove();
			}
		}

		this.removeArrows();

		if(this.timerId !== undefined)
		{
			this.log("Clearing timer");

			window.clearInterval(this.timerId);
		}

		window.removeEventListener("resize", this.resizeHandler);
		this.resizeObserver.disconnect();

		Slideshow.instances.delete(this.element);
	}

	/**
	 * Removes arrows from this slideshow
	 */
	removeArrows()
	{
		if(this.generatedArrows !== undefined)
		{
			this.log("Removing arrows");

			for(let arrow of this.generatedArrows)
			{
				arrow.remove();
			}
		}
	}
}