import Slideshow, {FoxyClassName} from "./Slideshow.js";

/**
 * The directions that an animation can move in
 */
export let FoxyDirection =
{
	forward: "foxy-direction-forward",
	backward: "foxy-direction-backward",
	default: "foxy-direction-default"
};

let defaultOptions = Object.assign(
{
	animation: "slide",
	bullets: true,
	bulletsParent: null,
	onMove: undefined,
	dragging: true
}, Slideshow.defaultOptions);

/**
 * The base foxy class, handles most of the slideshow logic
 */
export default class Foxy extends Slideshow
{
	static defaultOptions = defaultOptions;
	static baseClass = "foxy";

	/**
	 * Creates a new Foxy instance
	 * @param	{HTMLElement}	element		The element to apply foxy to
	 * @param	{Object}		options		The options to give the element
	 */
	constructor(element, options)
	{
		super(element, options);

		if(!this.element.classList.contains(this.constructor.baseClass))
		{
			this.element.classList.add(this.constructor.baseClass);
			console.warn(`Element missing base '${this.constructor.baseClass}' class, added automatically. This class should already be applied, to avoid unnecessary DOM updates.`);
		}

		this.element.classList.add(`foxy-${options.animation}`);

		if(this.options.dragging)
		{
			this.setupDragHandlers();
		}

		this.element.classList.add(FoxyClassName.noAnimation);
	}

	set(option, value)
	{
		let currentAnimation = this.options.animation;

		super.set(option, value);

		if(option === "animation")
		{
			this.element.classList.remove(`foxy-${currentAnimation}`);
			this.element.classList.add(`foxy-${value}`);
		}
		else if(["bullets", "bulletsParent"].includes(option))
		{
			this.removeBullets();
			if(this.options.bullets) this.setupBullets();
		}
		else if(option === "dragging")
		{
			this.removeDragHandlers();
			if(value) this.setupDragHandlers();
		}

		return this;
	}

	/**
	 * Triggers whenever the dimensions of the slideshow might have changed
	 */
	dimensionsUpdated()
	{
		super.dimensionsUpdated();

		this.setCssDimensions();

		if(this.options.bullets)
		{
			this.setupBullets();
		}
	}

	/**
	 * Passes the width of the container to CSS, for the purpose of animations
	 */
	setCssDimensions()
	{
		let width = this.element.clientWidth;
		let height = this.element.clientHeight;
		this.element.style.setProperty("--full-width", `${width}px`);
		this.element.style.setProperty("--full-height", `${height}px`);
	}

	/**
	 * Sets up, or resets, the slide bullets
	 */
	setupBullets()
	{
		this.log("Setting up bullets");

		let bulletsElement;

		if(this.options.bulletsParent === null)
		{
			this.log(`No bullets parent provided`);
			bulletsElement = this.element.getElementsByClassName(FoxyClassName.bullets).item(0);

			if(bulletsElement === null)
			{
				this.log("Creating new bullets parent");
				bulletsElement = document.createElement("div");
				bulletsElement.classList.add(FoxyClassName.bullets);
				this.element.appendChild(bulletsElement);
			}
		}
		else if(typeof this.options.bulletsParent === "string")
		{
			this.log(`CSS selector provided, searching for bullets parent element "${this.options.bulletsParent}`);
			bulletsElement = document.querySelector(this.options.bulletsParent);

			if(bulletsElement === null)
			{
				console.warn(`The bulletsParent selector '${this.options.bulletsParent} matched no elements on the page.`);
				return;
			}

			this.log("Found bullets parent element");
		}
		else if(this.options.bulletsParent instanceof HTMLElement)
		{
			bulletsElement = this.options.arrowsParent;
		}
		else
		{
			throw new Error("The bulletsParent option must be a DOM selector string, an HTMLElement or null");
		}

		if(this.generatedBullets !== undefined)
		{
			this.generatedBullets.forEach((bullet) => bullet.remove());
		}

		this.generatedBullets = [];

		for(let i = 0; i < this.getSlideCount(); i += 1)
		{
			let index = i;
			let listItem = document.createElement("li");
			let button = document.createElement("button");

			listItem.classList.add(FoxyClassName.bulletItem);
			button.classList.add(FoxyClassName.bullet);

			listItem.appendChild(button);
			bulletsElement.appendChild(listItem);

			button.innerText = String(i + 1);

			button.addEventListener("click", (event) =>
			{
				event.preventDefault();

				let ignored = this.moveTo(index);
			});

			this.generatedBullets.push(listItem);
		}

		this.updateCurrentBullet();
	}

	/**
	 * Sets up the drag handlers
	 */
	setupDragHandlers()
	{
		this.log("Setting up dragging support");

		let initialX = undefined;
		let initialY = undefined;
		let elementHeight = undefined;
		let elementWidth = undefined;
		let dragMultiplier = undefined;
		let initialSlide = undefined;
		let currentOffset = undefined;
		let startTime = undefined;

		let clamp = (min, value, max) => Math.min(Math.max(min, value), max);

		// Touch events can involve multiple fingers, so we'll use a slightly different method for accessing pageX/pageY
		let getPointFromEvent = (event) =>
		{
			if(event instanceof MouseEvent)
			{
				return [event.pageX, event.pageY];
			}
			else if(event instanceof TouchEvent)
			{
				for(let touch of event.touches)
				{
					if(touch.pageX !== undefined && touch.pageY !== undefined)
					{
						return [touch.pageX, touch.pageY];
					}
				}
			}

			return [0, 0];
		}

		let onDragStart = (event) =>
		{
			// Make sure we're not right-clicking here
			if(event.button !== undefined && event.button !== 0) return;

			this.log("Starting drag");

			[initialX, initialY] = getPointFromEvent(event);
			startTime = Date.now();

			elementWidth = this.element.getBoundingClientRect().width;
			elementHeight = this.element.getBoundingClientRect().height;

			dragMultiplier = 0;
			currentOffset = 0;
			initialSlide = this.getCurrentIndex();
			this.element.classList.add(FoxyClassName.dragging);

			if(this.timerId !== undefined)
			{
				this.log("Pausing timer");
				window.clearInterval(this.timerId);
			}
		};

		let onDrag = async (event) =>
		{
			if(initialX === undefined) return; // This event doesn't apply to this element

			let [pageX, pageY] = getPointFromEvent(event);
			let differenceX = initialX - pageX;
			let differenceY = initialY - pageY;

			let percentageX = differenceX / elementWidth;
			let percentageY = differenceY / elementHeight;

			dragMultiplier = clamp(-1, percentageX + percentageY, 1);
			let desiredOffset = currentOffset;

			// We need to be animating in a specific direction
			if(dragMultiplier > 0)
			{
				desiredOffset = 1;
			}
			else if(dragMultiplier < 0)
			{
				desiredOffset = -1;
				dragMultiplier = Math.abs(dragMultiplier);
			}

			while(desiredOffset > currentOffset)
			{
				this.log("Incrementing slide to match drag direction");

				await this.next();
				currentOffset += 1;
			}

			while(desiredOffset < currentOffset)
			{
				this.log("Decrementing slide to match drag direction");

				await this.previous();
				currentOffset -= 1;
			}

			this.log(`Setting dragMultiplier to ${dragMultiplier}`);
			this.element.style.setProperty("--drag-multiplier", String(dragMultiplier));
		};

		let onDragEnd = async (event) =>
		{
			if(initialX === undefined) return; // This event doesn't apply to this element

			this.log("Finishing drag");

			let [pageX, pageY] = getPointFromEvent(event);
			let differenceX = initialX - pageX;
			let differenceY = initialY - pageY;
			let timeDifference = Date.now() - startTime;

			// If the user has dragged the element more than half of the width of the element
			let isFullDrag = Math.abs(dragMultiplier) > 0.2;

			// If the user has made a "fling" gesture
			let isFling = Math.max(Math.abs(differenceX), Math.abs(differenceY)) > 50 && timeDifference < 300;

			initialX = undefined;
			initialY = undefined;
			startTime = undefined;

			if(!isFullDrag && !isFling)
			{
				this.log("Partial drag, resetting");

				// We're assuming that the user has given up on dragging, so we'll just reverse the animation
				await this.moveTo(initialSlide);
				this.log(`Setting dragMultiplier to ${dragMultiplier}`);
				this.element.style.setProperty("--drag-multiplier", String(1 - dragMultiplier));
			}

			this.element.classList.remove(FoxyClassName.dragging);
			dragMultiplier = undefined;
			this.resetTimer();
		};

		/** @var {Map<EventTarget, Map<string, (MouseEvent) => void>>} dragHandlers */
		let dragHandlers = new Map();
		dragHandlers.set(window, new Map());
		dragHandlers.set(this.element, new Map());

		dragHandlers.get(this.element).set("mousedown", onDragStart);
		dragHandlers.get(window).set("mousemove", onDrag);
		dragHandlers.get(window).set("mouseup", onDragEnd);

		dragHandlers.get(this.element).set("touchstart", onDragStart);
		dragHandlers.get(window).set("touchmove", onDrag);
		dragHandlers.get(window).set("touchend", onDragEnd);

		dragHandlers.forEach((events, target) =>
		{
			events.forEach((handler, event) => target.addEventListener(event, handler));
		});

		this.dragHandlers = dragHandlers;
	}

	/**
	 * Sets the current bullet
	 */
	updateCurrentBullet()
	{
		if(this.generatedBullets === undefined || this.generatedBullets.length === 0)
		{
			return;
		}

		this.log("Updating current bullet");

		let bulletItems = this.generatedBullets;
		let bullets = bulletItems.map((bulletItem) => bulletItem.children.item(0));

		for(let i = 0; i < bulletItems.length; i += 1)
		{
			bulletItems[i].classList.remove(FoxyClassName.activeBullet);
			bullets[i].disabled = false;
		}

		let currentIndex = this.getCurrentIndex();

		if(currentIndex !== undefined)
		{
			this.log(`Setting current bullet to bullet ${currentIndex}`);
			bulletItems[currentIndex].classList.add(FoxyClassName.activeBullet);
			bullets[currentIndex].disabled = true;
		}
	}

	/**
	 * Gets the elements in a particular slide
	 * @param	{number}			slide	A specific slide to limit the elements to
	 * @return	{HTMLElement[]}				The elements in that slide
	 */
	getElementsInSlide(slide)
	{
		let elements = this.getElements();

		return [elements[slide]];
	}

	/**
	 * Gets the number of slides in this slideshow
	 * @return	{number}	The number of slides
	 */
	getSlideCount()
	{
		return this.getElements().length;
	}

	/**
	 * Gets the number of elements per slide
	 * @return	{number}	The number of elements per slide
	 */
	getPerSlide()
	{
		return 1;
	}

	/**
	 * Gets the current elements
	 * @return	{HTMLElement[]}		The current elements
	 */
	getCurrent()
	{
		let current = this.element.getElementsByClassName(FoxyClassName.current);
		let result = [];

		for(let i = 0; i < current.length; i += 1)
		{
			result.push(current.item(i));
		}

		return result;
	}

	/**
	 * Gets the slide index of a specific element
	 * @param	{HTMLElement}		element		The elemeent to get the index for
	 * @return	{number|undefined}				The 0-based index, or undefined if that element isn't in this slideshow
	 */
	getIndex(element)
	{
		let elements = this.getElements();
		let index = elements.indexOf(element);

		if(index < 0)
		{
			return undefined;
		}

		return index;
	}

	/**
	 * Gets the index of the current slide
	 * @return	{number[]}	The 0-based indexes
	 */
	getCurrentIndex()
	{
		let current = this.getCurrent()[0];

		if(current === undefined)
		{
			return undefined;
		}

		return this.getIndex(current);
	}

	/**
	 * Moves to a specific slide
	 * @param	{HTMLElement|number|string}		slide		The element, or the slide index to move to
	 * @param	{string}						direction	The direction to move in
	 * @return	{Promise<void>}								After the move has completed
	 */
	async moveTo(slide, direction = FoxyDirection.default)
	{
		if(typeof slide === "number")
		{
			if(this.getSlideCount() > slide)
			{
				this.log(`Moving to slide ${slide}`);
				await this.moveTo(this.getElementsInSlide(slide)[0], direction);
			}
			else
			{
				this.log(`Slide ${slide} does not exist, skipping`);
			}

			return;
		}
		else if(typeof slide === "string")
		{
			this.log(`Using selector to search for slide "${slide}"`);

			/** @var {HTMLElement} */
			let actualElement = document.querySelector(slide);

			if(actualElement === undefined)
			{
				this.log("No slide found, skipping");
				return;
			}

			await this.moveTo(actualElement, direction);

			return;
		}

		let current = this.getCurrent();

		// Don't attempt to move to the current slide
		if(current[0] === slide)
		{
			this.log("Attempting to move to current slide, skipping");
			return;
		}

		// Remove the animation disabler, now that we've definitely moved to another slide
		this.element.classList.remove(FoxyClassName.noAnimation);

		// Make sure the effects of dragging have been removed
		this.log(`Setting dragMultiplier to 0`);
		this.element.style.setProperty("--drag-multiplier", String(0));

		let slideIndex = this.getIndex(slide);
		let currentIndex = this.getCurrentIndex();

		if(direction === FoxyDirection.default)
		{
			if(slideIndex === undefined || currentIndex === undefined || currentIndex < slideIndex)
			{
				this.log("Default animation selected. Moving forward");
				direction = FoxyDirection.forward;
			}
			else
			{
				this.log("Default animation selected. Moving backward");
				direction = FoxyDirection.backward;
			}
		}

		for(let key of Object.keys(FoxyDirection))
		{
			this.element.classList.remove(FoxyDirection[key]);
		}

		this.element.classList.add(direction);
		let elements = this.getElements();

		for(let element of elements)
		{
			element.classList.remove(FoxyClassName.departed);
		}

		this.resetTimer();

		for(let element of elements)
		{
			element.classList.remove(FoxyClassName.current);
		}

		if(current !== undefined)
		{
			for(let element of current)
			{
				element.classList.add(FoxyClassName.departed);
			}
		}

		let slideElements = this.getElementsInSlide(this.getIndex(slide));

		for(let element of slideElements)
		{
			element.classList.add(FoxyClassName.current);
		}

		if(this.options.onMove !== undefined)
		{
			this.options.onMove(currentIndex, slideIndex, direction);
		}

		this.updateCurrentBullet();
	}

	/**
	 * Moves to the next slide
	 * @return	{Promise<void>}		After the move has completed
	 */
	async next()
	{
		let currentIndex = this.getCurrentIndex();
		let total = this.getSlideCount();

		if(currentIndex === undefined || currentIndex >= total - 1)
		{
			this.log("Moving to first slide");
			await this.moveTo(0, FoxyDirection.forward);
		}
		else
		{
			this.log("Moving to next slide");
			await this.moveTo(currentIndex + 1, FoxyDirection.forward);
		}
	}

	/**
	 * Moves to the previous slide
	 * @return	{Promise<void>}		After the move has completed
	 */
	async previous()
	{
		let currentIndex = this.getCurrentIndex();
		let total = this.getSlideCount();

		if(currentIndex === undefined)
		{
			this.log("Moving to first slide");
			await this.moveTo(0, FoxyDirection.backward);
		}
		else if(currentIndex <= 0)
		{
			this.log("Moving to last slide");
			await this.moveTo(total - 1, FoxyDirection.backward);
		}
		else
		{
			this.log("Moving to previous slide");
			await this.moveTo(currentIndex - 1, FoxyDirection.backward);
		}
	}

	/**
	 * Removes all foxy classes from this element, deletes any foxy elements, and cleans up any dangling variables
	 */
	deinit()
	{
		super.deinit();

		let elements = this.getElements();

		for(let key in Object.keys(FoxyClassName))
		{
			this.element.classList.remove(FoxyClassName[key]);

			for(let element of elements)
			{
				element.classList.remove(FoxyClassName[key]);
			}
		}

		for(let key in Object.keys(FoxyDirection))
		{
			this.element.classList.remove(FoxyClassName[key]);
		}

		this.removeBullets();
		this.removeDragHandlers();
	}

	/**
	 * Removes bullets from this slideshow
	 */
	removeBullets()
	{
		if(this.generatedBullets !== undefined)
		{
			this.log("Removing bullets");

			for(let bullet of this.generatedBullets)
			{
				bullet.remove();
			}
		}
	}

	/**
	 * Removes drag handlers from this slideshow
	 */
	removeDragHandlers()
	{
		if(this.dragHandlers !== undefined)
		{
			this.dragHandlers.forEach((events, target) =>
			{
				events.forEach((handler, event) => target.removeEventListener(event, handler));
			});
		}
	}
}