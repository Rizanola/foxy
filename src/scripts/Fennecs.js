import Foxy from "./Foxy.js";
import {FoxyClassName} from "./Slideshow.js";

let defaultOptions = Object.assign(
{
	rows: undefined
}, Foxy.defaultOptions);

/**
 * Fennecs specific class, handles grid logic
 */
export default class Fennecs extends Foxy
{
	static defaultOptions = defaultOptions;
	static baseClass = "fennecs";

	/**
	 * Creates a new Foxy instance
	 * @param	{HTMLElement}	element		The element to apply foxy to
	 * @param	{Object}		options		The options to give the element
	 */
	constructor(element, options)
	{
		// Need to set this first
		if(options.rows !== undefined)
		{
			element.style.setProperty("--rows", options.rows);
		}

		if(options.bullets)
		{
			element.classList.add(FoxyClassName.hasBullets);
		}

		super(element, options);
	}

	set(option, value)
	{
		super.set(option, value);

		if(option === "rows")
		{
			this.reorganise();
		}

		return this;
	}

	/**
	 * Triggers whenever the dimensions of the slideshow might have changed
	 */
	dimensionsUpdated()
	{
		this.reorganise();

		super.dimensionsUpdated();
	}

	/**
	 * Gets the number of columns in a row
	 * @return	{Number}	The number of columns
	 */
	getColumns()
	{
		let computed = window.getComputedStyle(this.element).gridTemplateColumns;
		return computed.split(" ").length;
	}

	/**
	 * Gets the number of rows to display
	 * @return	{Number}	The number of rows
	 */
	getRows()
	{
		let rows = window.getComputedStyle(this.element).getPropertyValue("--rows");
		return rows === undefined ? 1 : Number(rows);
	}

	/**
	 * Reorganises elements
	 */
	reorganise()
	{
		this.log("Reorganising cells");

		let currentElements = this.getCurrent();

		let elements = this.getElements();

		// Reset slide overlap
		for(let slide of elements)
		{
			slide.style.setProperty("--row", "auto");
			slide.style.setProperty("--column", "auto");
		}

		let columns = this.getColumns();
		let perSlide = this.getPerSlide();

		this.log(`Generating a ${columns}x${this.getRows()} grid`);

		// Assign slides to overlapping cells
		elements.forEach((slide, index) =>
		{
			let repeatingIndex = index % perSlide;
			let column = repeatingIndex % columns;
			let row = (repeatingIndex - column) / columns;

			slide.style.setProperty("--row", String(row + 1));
			slide.style.setProperty("--column", String(column + 1));
		});

		// Handle applying and removing "current" and "departed" classes, so that the correct elements are currently visible
		let updatedElements = this.getElementsInSlide(this.getCurrentIndex());
		let departedElements = this.element.getElementsByClassName(FoxyClassName.departed);

		currentElements.forEach((element) => element.classList.remove(FoxyClassName.current, FoxyClassName.departed));
		updatedElements.forEach((element) => element.classList.add(FoxyClassName.current));

		// We only want the "departed" animation to apply to the number of elements in a slide
		for(let i = 0; i < Math.min(departedElements.length, currentElements.length - updatedElements.length); i += 1)
		{
			departedElements.item(i).removeAttribute(FoxyClassName.departed);
		}
	}

	/**
	 * Gets the number of elements per slide
	 * @return	{number}	The number of elements per slide
	 */
	getPerSlide()
	{
		let columns = this.getColumns();
		let rows = this.getRows();
		return rows * columns;
	}

	getElementsInSlide(slide)
	{
		let perSlide = this.getPerSlide();
		return this.getElements().slice(slide * perSlide, slide * perSlide + perSlide);
	}

	getSlideCount()
	{
		return Math.ceil(this.getElements().length / this.getPerSlide());
	}

	getIndex(element)
	{
		let baseIndex = super.getIndex(element);

		if(baseIndex === undefined)
		{
			return undefined;
		}

		return Math.floor(baseIndex / this.getPerSlide());
	}

	deinit()
	{
		for(let element of this.getElements())
		{
			element.style.setProperty("--row", "");
			element.style.setProperty("--column", "");
		}

		super.deinit();

		this.log("Removing resize handler and observer");
		this.resizeObserver.disconnect();
		window.removeEventListener("resize", this.resizeHandler);
	}
}