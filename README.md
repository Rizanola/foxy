# Foxy 🦊

Welcome to Foxy, documentation can be found under docs/

## Building

Foxy attempts to use require as little building as possible. The only build step is for generating a bundled browser version, for those who don't want to use ES6 imports.

This can be triggered using `yarn run build`.

## Documentation

Documentation for using Foxy can be found in the docs folder. Or by visiting the [documentation site](https://foxy.netlify.app/docs/).